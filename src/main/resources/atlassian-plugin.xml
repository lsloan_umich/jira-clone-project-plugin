<!--
  ~ Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
  ~  2.0 (the "License"); you may not use this file except in compliance with the License. You
  ~  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
  ~  required by applicable law or agreed to in writing, software distributed under the License is
  ~  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
  ~  either express or implied. See the License for the specific language governing permissions
  ~  and limitations under the License.
  -->

<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
		<param name="atlassian-data-center-compatible">true</param>
    </plugin-info>

	<!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="jira-clone-project-plugin"/>

    <!-- add our web resources -->
    <web-resource key="jira-clone-project-plugin-resources" name="jira-clone-project-plugin Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>

        <resource type="download" name="jira-clone-project-plugin.css" location="/css/jira-clone-project-plugin.css"/>
        <resource type="download" name="jira-clone-project-plugin.js" location="/js/jira-clone-project-plugin.js"/>
        <resource type="download" name="images/" location="/images"/>

        <context>jira-clone-project-plugin</context>
    </web-resource>

	<web-resource key="action-soy-templates">
		<transformation extension="soy">
			<transformer key="soyTransformer"/>
		</transformation>
		<transformation extension="js">
			<transformer key="jsI18n"/>
		</transformation>

		<dependency>com.atlassian.auiplugin:aui-experimental-soy-templates</dependency>

		<resource type="download" name="project-clone.js" location="templates/project-clone.soy"/>
	</web-resource>

	<webwork1 key="projectCloneActions" name="Project Clone Actions" class="java.lang.Object">
		<actions>
			<action name="com.atlassian.jira.plugins.projectclone.web.CloneProject" alias="CloneProject">
				<view name="error">/secure/admin/views/addproject.jsp</view>
				<view name="input">/secure/admin/views/addproject.jsp</view>
				<view name="security">/secure/views/securitybreach.jsp</view>
			</action>
		</actions>
	</webwork1>

    <!-- publish our component -->
    <!--<component key="myPluginComponent" class="com.atlassian.jira.plugins.projectclone.MyPluginComponentImpl" public="true">-->
        <!--<interface>com.atlassian.jira.plugins.projectclone.MyPluginComponent</interface>-->
    <!--</component>-->

    <!-- import from the product container -->
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />

	<web-item key="clone_project" name="Clone Project" section="system.view.project.operations"
			  i18n-name-key="webfragments.view.project.operations.item.clone.project.name"  weight="20">
		<label key="admin.projects.clone.project" />
		<link linkId="clone_project">/secure/project/CloneProject!default.jspa?pid=$helper.project.id</link>
		<condition class="com.atlassian.jira.plugin.webfragment.conditions.HasSelectedProjectCondition" />
		<conditions type="OR">
			<condition class="com.atlassian.jira.plugin.webfragment.conditions.UserIsAdminCondition" />
			<condition class="com.atlassian.jira.plugin.webfragment.conditions.HasProjectPermissionCondition">
				<param name="permission">project</param>
			</condition>
		</conditions>
	</web-item>

	<module-type key="project-settings-copier" class="com.atlassian.jira.plugins.projectclone.extensions.ProjectSettingsCopierModuleDescriptor">
		<description>
			This module type allows external plugins to provide an implementation of a project settings copier.
		</description>
	</module-type>

	<project-settings-copier key="assigneeCopier" class="com.atlassian.jira.plugins.projectclone.copiers.AssigneeTypeCopier" weight="1"/>
	<project-settings-copier key="componentsCopier" class="com.atlassian.jira.plugins.projectclone.copiers.ComponentsCopier" weight="2"/>
	<project-settings-copier key="versionsCopier" class="com.atlassian.jira.plugins.projectclone.copiers.VersionsCopier" weight="3"/>
	<project-settings-copier key="permissionSchemeCopier" class="com.atlassian.jira.plugins.projectclone.copiers.PermissionSchemeCopier" weight="3"/>
	<project-settings-copier key="notificationsSchemeCopier" class="com.atlassian.jira.plugins.projectclone.copiers.NotificationsSchemeCopier" weight="3"/>
	<project-settings-copier key="issueSecuritySchemeCopier" class="com.atlassian.jira.plugins.projectclone.copiers.IssueSecuritySchemeCopier" weight="3"/>
	<project-settings-copier key="workflowSchemeCopier" class="com.atlassian.jira.plugins.projectclone.copiers.WorkflowSchemeCopier" weight="3"/>
	<project-settings-copier key="fieldLayoutSchemeCopier" class="com.atlassian.jira.plugins.projectclone.copiers.FieldLayoutSchemeCopier" weight="3"/>
	<project-settings-copier key="projectRolesCopier" class="com.atlassian.jira.plugins.projectclone.copiers.ProjectRolesCopier" weight="3"/>
	<project-settings-copier key="customFieldsCopier" class="com.atlassian.jira.plugins.projectclone.copiers.CustomFieldsCopier" weight="3"/>
    <project-settings-copier key="issueTypeSchemeCopier" class="com.atlassian.jira.plugins.projectclone.copiers.IssueTypeSchemeCopier" weight="3"/>
    <project-settings-copier key="issueTypeScreenSchemeCopier" class="com.atlassian.jira.plugins.projectclone.copiers.IssueTypeScreenSchemeCopier" weight="3"/>

</atlassian-plugin>
