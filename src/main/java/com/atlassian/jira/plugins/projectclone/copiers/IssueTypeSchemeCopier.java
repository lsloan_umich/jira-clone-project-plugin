/*
 * Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
 *  2.0 (the "License"); you may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
 *  required by applicable law or agreed to in writing, software distributed under the License is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

package com.atlassian.jira.plugins.projectclone.copiers;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.context.manager.JiraContextTreeManager;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class IssueTypeSchemeCopier extends PermissionSchemeCopier {
	@Autowired
	protected IssueTypeSchemeManager issueTypeSchemeManager;

    @Autowired
    protected FieldConfigSchemeManager configSchemeManager;

    @Autowired
    protected FieldManager fieldManager;

	@Override
	public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
        final FieldConfigScheme fieldConfigScheme = issueTypeSchemeManager.getConfigScheme(project);

        if (fieldConfigScheme != null) {
            final FieldConfigScheme newConfigScheme = issueTypeSchemeManager.getConfigScheme(newProject);
            if (fieldConfigScheme.equals(newConfigScheme)) {
                // the same scheme, no need to copy it
                return;
            }

            final List<Project> projects = fieldConfigScheme.getAssociatedProjectObjects();
            projects.add(newProject);

            final Long[] projectIds = Iterables.<Long>toArray(Iterables.transform(projects, CustomFieldsCopier.PROJECT_TO_ID), Long.class);

            // Set the contexts
            final List contexts = CustomFieldUtils.buildJiraIssueContexts(false,
                    null,
                    projectIds,
                    ComponentAccessor.getComponentOfType(JiraContextTreeManager.class));

            configSchemeManager.updateFieldConfigScheme(fieldConfigScheme, contexts,
                    fieldManager.getConfigurableField(IssueFieldConstants.ISSUE_TYPE));

            fieldManager.refresh();
        }
	}
}
