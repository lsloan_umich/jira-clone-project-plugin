/*
 * Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
 *  2.0 (the "License"); you may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
 *  required by applicable law or agreed to in writing, software distributed under the License is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

package com.atlassian.jira.plugins.projectclone.extensions;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import org.dom4j.Element;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class ProjectSettingsCopierModuleDescriptor extends AbstractJiraModuleDescriptor<ProjectSettingsCopier> {

	private int weight = Integer.MAX_VALUE;

	protected ProjectSettingsCopierModuleDescriptor(JiraAuthenticationContext authenticationContext, ModuleFactory moduleFactory) {
		super(authenticationContext, moduleFactory);
	}

	public int getWeight()
	{
		return weight;
	}

	@Override
	public void init(Plugin plugin, Element element) throws PluginParseException
	{
		super.init(plugin, element);

		final String weightString = element.attributeValue("weight");
		if (weightString != null)
		{
			try
			{
				this.weight = Integer.valueOf(weightString);
			}
			catch (NumberFormatException e)
			{
				throw new PluginParseException(
						String.format("Invalid value for weight, must be an integer: %s", weight), e);
			}
		}
	}
}
